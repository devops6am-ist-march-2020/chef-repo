#
# Cookbook:: custom_tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

# This is where you will store a copy of your key on the chef-client
secret = Chef::EncryptedDataBagItem.load_secret("/etc/chef/encrypted_data_bag_secret")
 
# This decrypts the data bag contents of "mysecrets->marioworld" and uses the key defined at variable "secret"
tomcatsecret = Chef::EncryptedDataBagItem.load("mysecrets", "tomcatdetails", secret)




package 'java-1.8.0-openjdk.x86_64' do
 action :install
end

user node['tomcat']['user'] do
  comment 'tomcat app User'
  home node['tomcat']['homedir']
  system true
  shell '/bin/nologin'
end

group node['tomcat']['group'] do
  action :create
end

package 'wget' do
 action :install
end

ark 'tomcat' do
  url node['tomcat']['downloadurl']
  home_dir node['tomcat']['homedir']
  prefix_root node['tomcat']['prefixdir']
  owner node['tomcat']['user']
  version node['tomcat']['downloadversion']
end

execute 'GivingPermisions' do
 command 'chmod 755 /opt/app/bin/*.sh'
end

template "/opt/app/conf/tomcat-users.xml" do
  variables(:mytomcatusername => tomcatsecret['tomcatuser'],
            :mytomcatpassword => tomcatsecret['tomcatpassword'])
  source 'tomcat-users.xml.erb'
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/opt/app/webapps/manager/META-INF/context.xml" do
  source 'manager-context.xml.erb'
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/etc/systemd/system/tomcat.service" do
  source 'tomcat-init.erb'
  owner node['tomcat']['rootuser']
  group node['tomcat']['rootgroup']
  mode '0755'
  notifies :restart, 'service[tomcat]', :delayed
end

service 'tomcat' do
  supports :restart => true, :start => true, :stop => true 
  action [ :enable, :start ]
end






